<?php
/**
 * @version            2.3.2, plugin version 0.1a
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Calum Polwart, Derived from work by Tuan 
 * @copyright          Copyright (C) 2016 ShinySolutions / Calum Polwart
 * @license            GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;

class plgEventBookingSailwaveExtract extends JPlugin
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_eventbooking/table');
	}

	public function onEventDisplay($row)
	{
		ob_start();
		$this->displayExtractButton($row);
		$form = ob_get_contents();
		ob_end_clean();

		return array('title' => JText::_('Sailwave Extract'),
		             'form'  => $form
		);
	}

	/**
	 * Display button to allow event extract to be run
	 *
	 * @param object $row
	 */
	private function displayExtractButton($row)
	{
            //Do something
	}
}	